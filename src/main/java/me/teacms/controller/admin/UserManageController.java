package me.teacms.controller.admin;

import me.teacms.common.result.JsonResult;
import me.teacms.common.utils.AuthUtil;
import me.teacms.entity.User;
import me.teacms.entity.UserListPage;
import me.teacms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/21
 * Time: 21:46
 * Describe:
 */
@Controller
@RequestMapping(value = "/admin")
public class UserManageController {

    @Autowired
    private UserService userService;

    @RequestMapping(value="/user")
    public String  AllUser(){
        List<User> allUser=userService.findAllUserInfo();
        return "用户列表"+allUser;
    }

    @RequestMapping(value = "/user-manage.html", method = RequestMethod.GET)
    public String AllUsertest(){

/*        List<User> allUsertest=userService.findAllUserInfo();
        System.out.println("--------------------");
        System.out.println(allUsertest);
        System.out.println("--------------------");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("allUser", allUsertest);
        modelAndView.setViewName("_admin/user/testUser");
        return modelAndView;*/

        return "_admin/user/user_manage";
    }

     @RequestMapping(value = "/findUserList1")
     public UserListPage findListUsers(@RequestBody UserListPage user){
            UserListPage userListPage=userService.findAllUsers(user);
            if(userListPage==null){
                userListPage=new UserListPage();
            }
                return userListPage;
     }

     @ResponseBody
     @RequestMapping(value="/finduserlist/{id}")
     public JsonResult index(@PathVariable Integer id){
         int pageNum = 1, pageSize =5;
         JsonResult allUserCT = userService.findAllUserCT(id);
         return  allUserCT;
     }

     @RequestMapping(value = "/user/add",method = RequestMethod.POST)
     @ResponseBody
    public int addUser(User user){
         user.setUserStatus(1);
         user.setUserPass(AuthUtil.getPassword(user.getUserPass()));
         return userService.insert(user);
    }
    @RequestMapping(value = "/user/edit",method = RequestMethod.POST)
    @ResponseBody
    public int editUser(User user){
        user.setUserPass(AuthUtil.getPassword(user.getUserPass()));
        int i=userService.updateByPrimaryKeySelective(user);
        return i;
    }

    @RequestMapping(value = "/user/delete",method = RequestMethod.POST)
    @ResponseBody
    public int deleteUser(@RequestBody Integer id){
        int i=userService.updateUserStatus(id);
        return i;
    }


}
