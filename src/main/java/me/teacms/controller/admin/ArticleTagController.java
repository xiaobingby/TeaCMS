package me.teacms.controller.admin;

import me.teacms.common.result.JsonResult;
import me.teacms.entity.Tag;
import me.teacms.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/20
 * Time: 23:58
 * Describe: 文章标签
 */
@Controller
@RequestMapping(value = "/admin")
public class ArticleTagController {

    @Autowired
    private TagService tagService;

    @RequestMapping(value = "article-tags.html")
    public String articleTags() {

        return "_admin/article/article_tags";
    }

    @ResponseBody
    @RequestMapping(value="/findTagslist/{id}")
    public JsonResult index(@PathVariable Integer id){
        int pageNum = 1, pageSize =5;
        JsonResult allUserCT = tagService.findAllTagsCT(id);
        return  allUserCT;
    }

    @RequestMapping(value = "/tag/add",method = RequestMethod.POST)
    @ResponseBody
    public int addTag(Tag reTag){
        int i=tagService.insert(reTag);
        return i;
    }
    @RequestMapping(value = "/tag/edit",method = RequestMethod.POST)
    @ResponseBody
    public int editTag(Tag tag){
        int i=tagService.updateByPrimaryKeySelective(tag);
        return i;
    }

    @RequestMapping(value = "/tag/delete",method = RequestMethod.POST)
    @ResponseBody
    public int deleteTag(@RequestBody Integer id){
        int i=tagService.deleteTag(id);
        return i;
    }

}
