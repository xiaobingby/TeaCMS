package me.teacms.controller.admin;

import me.teacms.common.init.InitConfig;
import me.teacms.common.result.JsonResult;
import me.teacms.dao.WebConfigMapper;
import me.teacms.entity.WebConfig;
import me.teacms.entity.vo.BannerVo;
import me.teacms.entity.vo.Theme;
import me.teacms.service.BannerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/21
 * Time: 22:53
 * Describe:
 */
@Controller
@RequestMapping(value = "/admin")
public class SystemController {

    @Autowired
    private WebConfigMapper webconfigDao;
    @Autowired
    private BannerService bannerService;


    /**
     * Banner 视图
     *
     * @return
     */
    @RequestMapping(value = "/banner.html")
    public String banner(ModelMap modelMap) {

        List<BannerVo> allBannerArticle = bannerService.findAllBannerArticle();

        modelMap.put("allBannerArticle", allBannerArticle);
        return "_admin/system/banner";
    }

    /**
     * 查找所有Banner
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/findallbanner")
    public JsonResult findAllBanner() {
        JsonResult allBanner = bannerService.findAllBanner();

        return allBanner;
    }

    /**
     * 删除Banner
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/deletebanner/{id}")
    public Integer deleteBanner(@PathVariable Integer id) {

        Integer integer = bannerService.deleteBanner(id);

        return integer;
    }

    /**
     * 添加Banner
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/addbanner")
    public Integer addBanner(BannerVo bannerVo) {
        if (bannerVo.getOrder() == null) {
            return -1;
        }
        Integer integer = bannerService.addBanner(bannerVo);

        return integer;
    }

    /**
     * 更新Banner
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updatebanner")
    public Integer updateBanner(BannerVo bannerVo) {
        if (bannerVo.getOrder() == null) {
            return -1;
        }
        Integer integer = bannerService.updateBanner(bannerVo);

        return integer;
    }

    /**
     * 主题视图
     *
     * @return
     */
    @RequestMapping(value = "/theme.html")
    public String themes(HttpServletRequest request, ModelMap modelMap) {

        ArrayList<Theme> themes = null;


        //扫描文件夹 Start
        String basePath = request.getSession().getServletContext().getRealPath("/");
        basePath = basePath + "WEB-INF/views/_themes/";    //主题路径

        File[] listFiles = new File(basePath).listFiles();

        if (listFiles != null) { //如果不存在文件夹不执行
            themes = new ArrayList<Theme>();
            for (int i = 0; i < listFiles.length; i++) {    //循环取出所有文件夹
                if (listFiles[i].isDirectory()) {    //判断是不是文件夹	//设置Model值
                    Theme tempThemes = new Theme();
                    tempThemes.setThemeName(listFiles[i].getName());
                    tempThemes.setThemePath(listFiles[i].getName());
                    tempThemes.setScreenshot(listFiles[i].getName() + "/screenshot.png");
                    themes.add(tempThemes);
                }
            }

        }
        listFiles.clone();    //关闭流
        //扫描文件夹 End

        modelMap.put("webConfig", InitConfig.getWebConfig());
        modelMap.put("themes", themes);
        return "_admin/system/theme";
    }

    /**
     * 更新主题
     *
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updatetheme")
    public JsonResult updateTheme(@RequestBody String themeName) {
        JsonResult jsonResult = new JsonResult();

        WebConfig webConfig = InitConfig.getWebConfig();
        webConfig.setThemePath(themeName);
        int i = webconfigDao.updateByPrimaryKeySelective(webConfig);
        InitConfig.setWebConfig(webconfigDao.selectByPrimaryKey(1));
        if (i >= 1) {
            jsonResult.setSuccess(true);
        } else {
            jsonResult.setSuccess(false);
        }

        return jsonResult;
    }


    /**
     * 数据监控 视图
     *
     * @return
     */
    @RequestMapping(value = "/druid.html")
    public String druid() {

        return "_admin/system/druid";
    }


    /**
     * 性能监控 视图
     *
     * @return
     */
    @RequestMapping(value = "/monitoring.html")
    public String monitoring() {

        return "_admin/system/monitoring";
    }

    /**
     * 多说 视图
     *
     * @return
     */
    @RequestMapping(value = "/duoshuo.html")
    public String duoshuo() {

        return "_admin/system/duoshuo";
    }

    /**
     * 系统设置 视图
     *
     * @return
     */
    @RequestMapping(value = "/config.html")
    public String config(ModelMap modelMap) {

        WebConfig webConfig = webconfigDao.selectByPrimaryKey(1);

        modelMap.put("webConfig", webConfig);
        return "_admin/system/config";
    }

    /**
     * 更新网站配置
     *
     * @param webConfig
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/updatewebconfig")
    public Integer updateWebConfig(WebConfig webConfig) {

        int i = webconfigDao.updateByPrimaryKeySelective(webConfig);

        if (i >= 1) {
            InitConfig.setWebConfig(webconfigDao.selectByPrimaryKey(1));
            return 1;
        }

        return 0;
    }

}
