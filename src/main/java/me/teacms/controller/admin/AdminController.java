package me.teacms.controller.admin;

import com.google.code.kaptcha.servlet.KaptchaExtend;
import me.teacms.common.result.JsonResult;
import me.teacms.common.utils.AuthUtil;
import me.teacms.entity.User;
import me.teacms.entity.vo.UserVo;
import me.teacms.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/20
 * Time: 23:59
 * Describe: 用户登陆-用户注销
 */
@Controller
@RequestMapping(value = "/admin")
public class AdminController extends KaptchaExtend {

    @Autowired
    private AdminService adminService;

    @RequestMapping(value = {"", "/login.html"})
    public String login() {

        return "_admin/login";
    }

    /**
     * 生存验证码
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/code.jpg", method = RequestMethod.GET)
    public void captcha(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.captcha(req, resp);
    }

    /**
     * 登陆验证
     *
     * @param req
     * @param userVo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/login")
    public JsonResult login(HttpServletRequest req, UserVo userVo) {
        /**
         * 图片生成验证码
         */
        String kaptcha_session_key = getGeneratedKey(req);
        JsonResult jsonResult = new JsonResult();
        /**
         * 判断验证码是否正确
         */
        if (kaptcha_session_key == null || !kaptcha_session_key.equalsIgnoreCase(userVo.getCode())) {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("验证码错误");
            return jsonResult;
        }
        /**
         * 账号验证
         */
        User userPassIsCorrect = adminService.findUserPassIsCorrect(userVo);
        if (userPassIsCorrect == null) {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("账号或密码错误!");
            return jsonResult;
        } else {
            req.getSession().setAttribute("user", userPassIsCorrect);
            jsonResult.setSuccess(true);
            return jsonResult;
        }

    }

    /**
     * 用户登出
     * @param req
     * @return
     */
    @RequestMapping(value = "/logout")
    public void logout(HttpServletRequest req, HttpServletResponse res) throws IOException {

        //session过期
        req.getSession().invalidate();

        res.sendRedirect("/admin/login.html");
    }

    /**
     * 修改密码
     * @param session
     * @param psaa1
     * @param psaa2
     * @param psaa3
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/alertpass")
    public JsonResult alertPass(HttpSession session, String psaa1, String psaa2, String psaa3) {
        JsonResult jsonResult = new JsonResult();

        if (psaa1 == null || "".equals(psaa1)) {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("请输入原密码!");
            return jsonResult;
        }
        if (psaa2 == null) {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("请输入密码!");
            return jsonResult;
        }
        if (psaa3 == null || "".equals(psaa3)) {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("请输入确认密码!");
            return jsonResult;
        }
        if (!psaa2.equals(psaa3) || "".equals(psaa3)) {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("输入密码不一致!");
            return jsonResult;
        }

        User user = (User) session.getAttribute("user");
        String password = AuthUtil.getPassword(psaa1);
        if (user.getUserPass().equals(password)) {

            jsonResult = adminService.updateUserPass(session, jsonResult, user, psaa2);

            return jsonResult;
        } else {
            jsonResult.setSuccess(false);
            jsonResult.setMsg("原密码错误!");
            return jsonResult;
        }

    }

}
