package me.teacms.controller.admin;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import me.teacms.common.result.JsonResult;
import me.teacms.entity.Category;
import me.teacms.service.CategoryService;
import me.teacms.service.CommonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/20
 * Time: 23:57
 * Describe: 文章分类
 */
@Controller
@RequestMapping(value = "/admin")
public class ArticleCategoryCotroller {

    @Autowired
    private CommonService commonService;
    @Autowired
    private CategoryService categoryService;

    @RequestMapping(value = "article-category.html")
    public String articleCategory() {

        return "_admin/article/article_category";
    }

    /**
     * 获取文章分类
     * 使用 PageHelper插件
     */
    @ResponseBody
    @RequestMapping(value = "/getarticlecategory/{id}")
    public JsonResult getArticleCategory(@PathVariable Integer id) {

        PageHelper.startPage(id, 6);
        List<Category> allCategory = commonService.findAllCategory();
        PageInfo pageInfo = new PageInfo(allCategory);

        JsonResult jsonResult = new JsonResult();
        jsonResult.setObj(pageInfo);
        return jsonResult;
    }


    /**
     * 增加分类
     */

    @RequestMapping(value = "/category/add",method = RequestMethod.POST)
    @ResponseBody
    public int addCategory(Category category){
        int i=categoryService.insert(category);
        return i;
    }

    @RequestMapping(value = "/category/edit",method = RequestMethod.POST)
    @ResponseBody
    public int editCategory(Category category){
        int i=categoryService.updateByPrimaryKeySelective(category);
        return i;
    }

    @RequestMapping(value = "/category/delete",method = RequestMethod.POST)
    @ResponseBody
    public int deleteCategory(@RequestBody Integer id){
        int i=categoryService.deleteCategory(id);
        System.out.println(i);
        System.out.println(id);
        return i;
    }


}
