package me.teacms.common.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/2
 * Time: 1:51
 * Describe: MD5 加密解密
 */
public class AuthUtil {


    /**
     * 生产密文密码
     * @param password  明文密码
     * @return
     */
    public static String getPassword(String password) {
        return DigestUtils.md5Hex(password).toLowerCase();
    }


    /**
     * 生成密码密文
     * @param str
     * @return
     */
    public static String MD5(String str) {
        return DigestUtils.md5Hex(str).toLowerCase();
    }



}
