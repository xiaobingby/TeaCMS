package me.teacms.entity.vo;

import me.teacms.entity.User;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/26
 * Time: 15:04
 * Describe: 文章搜索 Entity
 */
public class SearchInfo {

    /**
     * 当前页
     */
    private Integer pageId;

    /**
     * 搜索信息
     */
    private String searchInfo;

    /**
     * 用户信息
     */
    private User user;

    public Integer getPageId() {
        return pageId;
    }

    public void setPageId(Integer pageId) {
        this.pageId = pageId;
    }

    public String getSearchInfo() {
        return searchInfo;
    }

    public void setSearchInfo(String searchInfo) {
        this.searchInfo = searchInfo;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
