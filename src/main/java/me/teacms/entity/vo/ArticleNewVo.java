package me.teacms.entity.vo;

import me.teacms.entity.Article;
import me.teacms.entity.ArticleCategory;
import me.teacms.entity.ArticleTag;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/22
 * Time: 21:09
 * Describe: 添加文章增强 Entity
 */
public class ArticleNewVo extends Article {

    private List<ArticleCategory> articleCategories;

    private List<ArticleTag> articleTags;

    public List<ArticleCategory> getArticleCategories() {
        return articleCategories;
    }

    public void setArticleCategories(List<ArticleCategory> articleCategories) {
        this.articleCategories = articleCategories;
    }

    public List<ArticleTag> getArticleTags() {
        return articleTags;
    }

    public void setArticleTags(List<ArticleTag> articleTags) {
        this.articleTags = articleTags;
    }
}
