package me.teacms.service;

import me.teacms.common.result.JsonResult;
import me.teacms.entity.Tag;

/**
 * Author: xiehuan
 * Email: 1487471733@qq.com
 * Date: 2017/2/27
 * Time: 14:36
 * Describe:
 */
public interface TagService {



    public JsonResult findAllTagsCT(Integer id);

    //add tags information
    int insert(Tag record);

    //edit tag infomation
    int updateByPrimaryKeySelective(Tag record);

    //多表删除标签
    int deleteTag(Integer id);
}
