package me.teacms.service;

import me.teacms.entity.Category;

/**
 * Author: xiehuan
 * Email: 1487471733@qq.com
 * Date: 2017/2/28
 * Time: 14:29
 * Describe:
 */

public interface CategoryService {
     int insert(Category record);

     int updateByPrimaryKeySelective(Category record);

     //多表联合删除分类
     int deleteCategory(Integer id);
}
