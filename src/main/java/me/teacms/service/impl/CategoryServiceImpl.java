package me.teacms.service.impl;

import me.teacms.dao.CategoryMapper;
import me.teacms.entity.Category;
import me.teacms.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Author: xiehuan
 * Email: 1487471733@qq.com
 * Date: 2017/2/28
 * Time: 14:30
 * Describe:
 */

@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    //增加一条分类记录
    @Override
    public int insert(Category record) {
        int i=categoryMapper.insert(record);
        return i;
    }

    @Override
    public int updateByPrimaryKeySelective(Category record) {
        int i=categoryMapper.updateByPrimaryKeySelective(record);
        return i;
    }

    @Override
    public int deleteCategory(Integer id) {
        int i=categoryMapper.deleteCategory(id);
        return i;
    }
}
