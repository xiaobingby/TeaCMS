package me.teacms.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import me.teacms.common.result.JsonResult;
import me.teacms.dao.UserMapper;
import me.teacms.dao.UsersMapper;
import me.teacms.entity.User;
import me.teacms.entity.UserListPage;
import me.teacms.entity.Users;
import me.teacms.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by XiaoBingBy on 2017/2/6.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UsersMapper usersMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return userMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(User record) {
        return userMapper.insert(record);
    }

    public Users findAllUser() {
        Users users = usersMapper.selectByPrimaryKey((long) 1);
        return users;
    }

    @Override
    public int updateByPrimaryKeySelective(User record) {
        return userMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public List<User> findAllUserInfo() {
        return userMapper.findAllUserInfo();
    }

    @Override
    public UserListPage findAllUsers(UserListPage users) {
        //查询所有标签
        List<User> userList=userMapper.findAllUsers(users);
        //潮汛标签总条数
        int allDataSize=userMapper.findAllUserDataSize();

        UserListPage userListPage=new UserListPage();
        User user=new User();

        if(allDataSize%users.getPageSize()==0){
            allDataSize=allDataSize/users.getPageSize();
        }else{
            allDataSize=allDataSize/users.getPageSize()+1;
        }
        userListPage.setUserList(userList);
        userListPage.setPageSize(allDataSize);
        return userListPage;
    }

    @Override
    public int updateUserStatus(Integer id) {

        Integer integer = userMapper.updateUserStatus(id);

        return integer;
    }

    //利用pagehelper分页查询用户信息
    @Override
    public JsonResult findAllUserCT(Integer id) {

        PageHelper.startPage(id,6);
        List<User> findAllUserCT=userMapper.findAllUserInfo();
        PageInfo<User> pageInfo=new PageInfo<>(findAllUserCT);
        JsonResult jsonResult = new JsonResult();

        jsonResult.setObj(pageInfo);
        jsonResult.setSuccess(true);
        return jsonResult;
    }

}
