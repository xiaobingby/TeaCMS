package me.teacms.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import me.teacms.common.init.InitConfig;
import me.teacms.dao.ArticleAllDao;
import me.teacms.dao.IndexDao;
import me.teacms.entity.Article;
import me.teacms.entity.vo.ArticleAllVo;
import me.teacms.service.IndexService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/6
 * Time: 15:51
 * Describe:
 */
@Service
public class IndexServiceImpl implements IndexService {

    @Autowired
    private IndexDao indexDao;
    @Autowired
    private ArticleAllDao articleAllDao;


    /**
     * 查询当前文章的所有分类和标签
     * 操作当前对象
     * ArticleAllServiceImpl 业务类中使用
     * @return
     */
    private void findThisArticleCategoryTag(ArticleAllVo item) {

            item.setCategories(articleAllDao.findAllCategory(item.getId()));
            item.setTags(articleAllDao.findAllTag(item.getId()));

        return;
    }

    /**
     *
     * @param pageNum
     * @return
     */
    @Override
    public PageInfo findArticlePage(Integer pageNum) {

        PageHelper.startPage(pageNum, InitConfig.getWebConfig().getPagesize());
        List<ArticleAllVo> articleAllVoList = indexDao.findArticlePage();

        /**
         * 查询文章分类-->文章标签
         */

        PageInfo pageInfo = new PageInfo(articleAllVoList);

        return pageInfo;
    }

    /**
     * 搜索文章
     * @param name
     * @return
     */
    @Override
    public List<ArticleAllVo> findSo(String name) {

        List<ArticleAllVo> so = indexDao.findSo(name);

        return so;
    }

    /**
     * 文章内容
     * @param id
     * @return
     */
    @Override
    public ArticleAllVo findArticleById(Integer id) {

        ArticleAllVo articleById = indexDao.findArticleById(id);
        if (articleById != null) {
            indexDao.updateArticlePageView(id);
            findThisArticleCategoryTag(articleById);
        }

        /*更新文章+1*/

        /**
         * 查询文章分类-->文章标签
         */

        return articleById;
    }

    @Override
    public List<Article> findNewArticleTop6() {

        List<Article> newArticleTop5 = indexDao.findNewArticleTop6();

        return newArticleTop5;
    }

    @Override
    public List<Article> findViewArticleTop6() {

        List<Article> viewArticleTop5 = indexDao.findViewArticleTop6();

        return viewArticleTop5;
    }

    /**
     * 点赞+1
     */
    @Override
    public Integer updateArticlePraise(Integer id) {
        int i = indexDao.updateArticlePraise(id);
        return i;
    }
}
