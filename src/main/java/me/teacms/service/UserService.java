package me.teacms.service;

import me.teacms.common.result.JsonResult;
import me.teacms.entity.User;
import me.teacms.entity.UserListPage;
import me.teacms.entity.Users;

import java.util.List;

/**
 * Created by XiaoBingBy on 2017/2/6.
 */
public interface UserService {

    //删除用户
    int deleteByPrimaryKey(Integer id);
    //增加一条用户
    int insert(User record);

    public Users findAllUser();

    //update user information
    int updateByPrimaryKeySelective(User record);

    List<User>  findAllUserInfo();
    //查询所有
    UserListPage findAllUsers(UserListPage users);

    //更新用户状态为0
    public int updateUserStatus(Integer id);

    //利用pagehelper分页查询用户信息

    public JsonResult findAllUserCT(Integer id);

}
