package me.teacms.service;

import me.teacms.entity.Category;
import me.teacms.entity.Tag;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/22
 * Time: 16:46
 * Describe: 公共业务方法
 */
public interface CommonService {

    /**
     * 查询所有分类
     * @return
     */
    public List<Category> findAllCategory();

    /**
     * 查询所有标签
     * @return
     */
    public List<Tag> findAllTag();

}
