package me.teacms.service;

import me.teacms.common.result.JsonResult;
import me.teacms.entity.vo.SearchInfo;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/24
 * Time: 17:28
 * Describe: 所有文章业务方法
 */
public interface ArticleAllService {

    /**
     * 查询所有文章信息
     * @param searchInfo 传入当前分页数 和 搜索信息
     * @return
     */
    public JsonResult findArticleAll(SearchInfo searchInfo);

    /**
     * 查询所有回收站文章 并 INNER JOIN 文章作者名
     * @param searchInfo 传入当前分页数 和 搜索信息
     * @return
     */
    public JsonResult findAllTrashCanArticle(SearchInfo searchInfo);

    /**
     * 逻辑删除文章
     * 更新文章状态
     * @param id    传入文章ID
     * @return
     */
    public JsonResult deleteLogicArticle(Integer id);

    /**
     * 恢复文章到所有文章中
     * @param id    传入文章ID
     * @return
     */
    public JsonResult updateArticleState(Integer id);

    /**
     * 彻底删除文章
     * @param id 传入文章ID
     * @return
     */
    public JsonResult deleteArticle(Integer id);

}
