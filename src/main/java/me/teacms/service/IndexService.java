package me.teacms.service;

import com.github.pagehelper.PageInfo;
import me.teacms.entity.Article;
import me.teacms.entity.vo.ArticleAllVo;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/6
 * Time: 15:50
 * Describe:
 */
public interface IndexService {

    /**
     *  文章主页 和 文章分页 业务接口
     * @param pageNum
     * @return
     */
    public PageInfo findArticlePage(Integer pageNum);

    /**
     * 搜索文章
     * @param name
     * @return
     */
    public List<ArticleAllVo> findSo(String name);

    /**
     * 文章内容 业务接口
     * @param id
     * @return
     */
    public ArticleAllVo findArticleById(Integer id);

    /**
     * 查询最新5条文章
     * @return
     */
    List<Article> findNewArticleTop6();

    /**
     * 查询浏览量最高5条文章
     * @return
     */
    List<Article> findViewArticleTop6();

    /**
     * 点赞+1
     * @return
     */
    public Integer updateArticlePraise(Integer id);

}
