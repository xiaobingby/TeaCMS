package me.teacms.dao;

import me.teacms.entity.Category;
import me.teacms.entity.Tag;
import me.teacms.entity.vo.ArticleAllVo;
import me.teacms.entity.vo.SearchInfo;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/2/24
 * Time: 17:33
 * Describe: 所有文章Dao
 */
public interface ArticleAllDao {

    /**
     * 查询所有文章 并 INNER JOIN 文章作者名
     * @param searchInfo 传入文章名
     * @return
     */
    List<ArticleAllVo> findArticleAll(SearchInfo searchInfo);

    /**
     * 回收站 trashCan
     * 查询所有回收站文章 并 INNER JOIN 文章作者名
     * 传入文章名
     * @param searchInfo 传入文章名
     * @return
     */
    List<ArticleAllVo> findAllTrashCanArticle(SearchInfo searchInfo);


    /**
     * 传入文章 ID 查询文章分类
     * @return
     */
    List<Category> findAllCategory(Integer id);

    /**
     * 传入文章 ID 查询文章标签
     * @return
     */
    List<Tag> findAllTag(Integer id);

    /**
     * 逻辑删除文章
     * 更新文章状态
     * @param id    传入文章ID
     * @return
     */
    int updateLogicArticle(Integer id);

    /**
     * 恢复文章到所有文章中
     * @param id    传入文章ID
     * @return
     */
    int updateArticleState(Integer id);

    /**
     * 彻底删除文章
     * @param id
     * @return
     */
    int deleteArticle(Integer id);

    /**
     * 彻底删除文章分类
     * 传入文章ID
     * @param id
     * @return
     */
    int deleteArticleCategory(Integer id);

    /**
     * 彻底删除文章标签
     * 传入文章ID
     * @param id
     * @return
     */
    int deleteArticleTag(Integer id);

}
