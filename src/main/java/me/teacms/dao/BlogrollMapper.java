package me.teacms.dao;

import me.teacms.entity.Blogroll;

public interface BlogrollMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Blogroll record);

    int insertSelective(Blogroll record);

    Blogroll selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Blogroll record);

    int updateByPrimaryKey(Blogroll record);
}