package me.teacms.dao;

import me.teacms.entity.vo.ArticleAllVo;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/8
 * Time: 6:45
 * Describe: 文章分类和标签 Dao 接口
 */
public interface CategoryAndTagDao {

    /**
     * 查询分类的文章
     * @param value 分类别名
     * @return
     */
    List<ArticleAllVo> findAllCategoryArticle(String value);

    /**
     * 查询标签的文章
     * @param value 标签别名
     * @return
     */
    List<ArticleAllVo> findAllTagArticle(String value);

}
