package me.teacms.dao;

import me.teacms.entity.Article;
import me.teacms.entity.vo.ArticleAllVo;

import java.util.List;

/**
 * Author: XiaoBingBy
 * Email: XiaoBingBy@qq.com
 * Date: 2017/3/6
 * Time: 12:02
 * Describe: 博客主页 Dao 接口
 */
public interface IndexDao {

    /**
     * 文章主页 和 文章分页
     * @return
     */
    List<ArticleAllVo> findArticlePage();

    /**
     * 文章搜索
     * @param value
     * @return
     */
    List<ArticleAllVo> findSo(String value);

    /**
     * 文章内容
     * @param id
     * @return
     */
    ArticleAllVo findArticleById(Integer id);

    /**
     * 首页幻灯片
     * @return
     */
    List<ArticleAllVo> findBlogroll();

    /**
     * 查询最新5条文章
     * @return
     */
    List<Article> findNewArticleTop6();

    /**
     * 查询浏览量最高5条文章
     * @return
     */
    List<Article> findViewArticleTop6();

    /**
     * 更新文章浏览量+1
     * @param value 文章ID
     * @return
     */
    int updateArticlePageView(Integer value);

    /**
     * 文章点赞+1
     * @param value 文章ID
     * @return
     */
    int updateArticlePraise(Integer value);

}
