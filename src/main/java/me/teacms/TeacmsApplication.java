package me.teacms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeacmsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeacmsApplication.class, args);
    }

}
