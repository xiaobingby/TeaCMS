package me.teacms.config;

import me.teacms.interceptor.UserInterceptor;
import me.teacms.interceptor.WebConfigInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * author :xie
 * Email: 1487471733@qq.com
 * Date: 2020/8/18 23:21
 * Describe: 
 */
@Configuration
public class WebAppConfigurer implements WebMvcConfigurer {

  @Bean
  public HandlerInterceptor getMyInterceptor(){
    return new WebConfigInterceptor();
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    // 可添加多个
    registry.addInterceptor(getMyInterceptor())
            .addPathPatterns("/**")
    .excludePathPatterns("/static/**","/views/**");
    registry.addInterceptor(new UserInterceptor())
            .addPathPatterns("/admin/*")
            .excludePathPatterns("/admin/login.html")
            .excludePathPatterns("/admin/code.jpg")
            .excludePathPatterns("/admin/login");
  }
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
//静态资源释放
    registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
    registry.addResourceHandler("/views/TeaCMS/**").addResourceLocations("classpath:/views/_themes/TeaCMS/");
  }
}
