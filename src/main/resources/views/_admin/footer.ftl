<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 0.0.1
    </div>
    <strong>Copyright &copy; 2016-2017 <a href="http://www.teacms.me">TeaCMS</a>.</strong> All rights
    reserved.
</footer>