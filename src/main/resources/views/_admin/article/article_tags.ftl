<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TeaCMS-后台管理</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="/static/plugins/pace-0.7.5/pace.js"></script>
    <link href="/static/plugins/pace-0.7.5/themes/black/pace-theme-center-circle.css" rel="stylesheet" />
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="/static/css/bootstrap.min.css">
    <!--bootstrap-validator-->
    <link rel="stylesheet" href="/static/plugins/bootstrap-validator/css/bootstrapValidator.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/static/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/static/css/skins/my_all-skins.css">
    <!-- MyAdminLTE.css -->
    <link rel="stylesheet" href="/static/css/MyAdminLTE.css">
    <!-- Lobibox -->
    <link rel="stylesheet" href="/static/plugins/lobibox/css/lobibox.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="//cdn.bootcss.com/owl-carousel/1.32/owl.carousel.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini fixed">
<#import "/_admin/lib/sidebar_templet.ftl" as sidebar_templet>
<!-- Site wrapper -->
<div class="wrapper">
<#-- header -->
<#include "/_admin/header.ftl">
<#-- sidebar -->
<@sidebar_templet.sidebar "article-tags"></@sidebar_templet.sidebar>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                文章标签
                <small>Article Tags</small>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content" id="app">

            <!-- Default box -->
            <div class="col-md-5" style="padding: 0">
                <div class="box box-primary">
                    <div class="box-body">
                    <div id="addTagOff" onclick="controlToggle()"><i id="rev" class='fa fa-minus-square'></i><i id="add" class='fa fa-plus-square'></i>添加</div>
                    <form id="addForm" method="post" action="#">
                        <h3 style="padding-top:20px;"><strong>添加标签</strong></h3>
                        <div class="form-group">
                            <div class="form-group">
                                <label class="form-inline">名称</label>
                                <input class="form-control" placeholder="" id="addName" name="name" autocomplete="off" >
                                <small class="form-inline">这将是它在站点上显示的名字。</small>
                            </div>
                            <div class="form-group">
                                <label class="form-inline">别名</label>
                                <input class="form-control" placeholder="" id="addAlias" name="alias" autocomplete="off">
                                <small>“别名”是在URL中使用的别称，它可以令URL更美观。通常使用小写，只能包含字母，数字和连字符（-）。</small>
                            </div>
                            <div class="box-footer clearfix">
                                <button class="btn btn-success pull-right">添加</button>
                            </div>
                        </div>
                    </form>
                    <div id="editTagOff" onclick="editToggle()"><i id="edit_rev" class='fa fa-minus-square'></i><i id="edit_add" class='fa fa-plus-square'></i>修改</div>
                    <form id="editForm" method="post" action="#">
                        <h3 style="padding-top:20px;"><strong>修改标签</strong></h3>
                        <div class="box-body">
                            <div class="form-group">
                            <div class="form-group" style="display: none">
                                <label class="form-inline">id</label>
                                <input class="form-control" placeholder="" id="editId" name="id" autocomplete="off">
                                <small class="form-inline">这将是它在站点上显示的id。</small>
                            </div>
                            <div class="form-group">
                                <label class="form-inline">名称</label>
                                <input class="form-control" placeholder="" id="editName" name="name" autocomplete="off">
                                <small class="form-inline">这将是它在站点上显示的名字。</small>
                            </div>
                            <div class="form-group">
                                <label class="form-inline">别名</label>
                                <input class="form-control" placeholder="" id="editAlias" name="alias" autocomplete="off">
                                <small>“别名”是在URL中使用的别称，它可以令URL更美观。通常使用小写，只能包含字母，数字和连字符（-）。</small>
                            </div>
                        </div>
                        <div class="box-footer clearfix">
                            <button class="btn btn-success pull-right">修改</button>
                        </div>
                            </div>
                    </form>
                    </div>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box box-primary">

                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>标签名</th>
                                    <th>别名</th>
                                    <th>文章数量</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="el in dataTag.list" :id="el.id">
                                    <td>{{el.id}}</td>
                                    <td>{{el.name}}</td>
                                    <td>{{el.alias}}</td>
                                    <td>{{el.count}}</td>
                                    <td>
                                        <button class="btn btn-xs btn-primary" v-on:click="editTag(el)">修改</button>
                                        <button class="btn btn-xs btn-danger" v-on:click="deleteTag(el.id)" >删除</button>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box-footer clearfix">
                    <ul class="pagination no-margin pull-right">
                        <li v-if="!dataTag.isFirstPage"><a href="javscript:;" v-on:click="getAllTag(dataTag.prePage)">«</a></li>
                        <li v-if="dataTag.isFirstPage" class="disabled"><a href="javscript:;">«</a></li>
                        <li v-for="item in dataTag.navigatepageNums" :class="item == dataTag.pageNum? 'active':''"><a href="javascript:void(0);" v-text="item" v-on:click="getAllTag(item)"></a></li>
                        <li v-if="!dataTag.isLastPage"><a href="javscript:;" v-on:click="getAllTag(dataTag.nextPage)">»</a><b></b></li>
                        <li v-if="dataTag.isLastPage" class="disabled"><a href="javscript:;">»</a></li>
                    </ul>
                </div>
                <div>
                    <h4><strong>注：</strong><br></h4>

                    <p>标签可以有选择性地转换成分类目录，请使用标签到分类目录转换器。</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

<#include "/_admin/footer.ftl">

    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="/static/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="/static/js/bootstrap.min.js"></script>
<!--vue-->
<script src="/static/plugins/vue/vue.js"></script>
<!-- SlimScroll -->
<script src="/static/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!--bootstrap-validator-->
<script src="/static/plugins/bootstrap-validator/js/bootstrapValidator.js"></script>
<!-- FastClick -->
<script src="/static/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="/static/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/static/js/demo.js"></script>
<script src="/static/plugins/lobibox/js/lobibox.min.js"></script>

<script src="/static/js/common.js"></script>

<script>

    $(document).ready(function() {
        $("#editForm").hide();
        $("#addForm").hide();
    });
    var addOff = true;
    var editOff = false;
    var clickEditIs = false;
    //custom select box
    $(function() {
        $("#add").hide();
        $("#rev").show();
        $("#edit_add").show();
        $("#edit_rev").hide();
        $("#editForm").hide();
        $("#addForm").show(200);
        getAllTag(0);
        addFormCheck();
        editFormCheck();
    });

    function deletMSG(type , msg) {
        Lobibox.notify(type, {
            size: 'mini',
            soundPath: '/static/plugins/lobibox/sounds/',
            position: 'center bottom',
            msg: msg
        });
    }

    var vm=new Vue({
        el:'#app',
        data: {
            dataTag: {}
        },
        methods: {
            editTag:function(tagInfo){
                showEditTag(tagInfo);
            },
            deleteTag:function (id) {
                showDeleteTag(id);
            },
            getAllTag:function(id){
                getAllTag(id);
            }


        }
    });
    //查询所有标签信息
    function getAllTag(id) {
        $.get("/admin/findTagslist/"+id, function(result){
            console.log(JSON.stringify(result));
            vm.dataTag = result.obj;
        });
    }
    //增加标签
    function successFn(data) {
        deletMSG('success', '添加成功!');
    }
    function errorFn(data) {
        deletMSG('error', '网络错误，请刷新!');
    }

    function addSubmit(){
        ajaxSubmitForm("/admin/tag/add",$("#addForm").serialize(),success,errorFn);
        function success(data){
            getAllTag(0);
            deletMSG('success', '操作成功!');
        }

    }

    //edit tag
    function editSubmit(){
        ajaxSubmitForm("/admin/tag/edit",$("#editForm").serialize(),success1,errorFn);
         function success1(data) {
             deletMSG('success', '操作成功!');
            editTagAfter();
        }
    }
    function  editTagAfter() {
        $("#editForm").data('bootstrapValidator').destroy();
        $('#editForm').data('bootstrapValidator', null);
        editFormCheck();
    }

    //点击编辑后执行方法
    function showEditTag(tagInfo){
        $("#editId").val(tagInfo.id);
        $("#editName").val(tagInfo.name);
        $("#editAlias").val(tagInfo.alias);
        $("#rev").hide(200);
        $("#add").show(200);
        $("#editForm").show(200);
        $("#addForm").hide(200);
        $("#editName").removeAttr("disabled");
        $("#editAlias").removeAttr("disabled");
        clickEditIs = true;
        addOff = false;
    }
    
    //点击删除后执行的方法
    function  showDeleteTag(id) {
        ajaxSubmit("/admin/tag/delete",JSON.stringify(id),sucess1,errorFn);
        function sucess1(data){
            $("#"+id).remove();
            deletMSG('success', '删除成功!');
        }

    }












</script>
</body>
</html>