/*
Navicat MySQL Data Transfer

Source Server         : 5828484357b83.gz.cdb.myqcloud.com
Source Server Version : 50628
Source Host           : 5828484357b83.gz.cdb.myqcloud.com:14259
Source Database       : teacms

Target Server Type    : MYSQL
Target Server Version : 50628
File Encoding         : 65001

Date: 2017-03-17 17:11:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article
-- ----------------------------
DROP TABLE IF EXISTS `article`;
CREATE TABLE `article` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键(ID)',
  `article_title` varchar(100) DEFAULT NULL COMMENT '标题',
  `article_author` int(20) unsigned DEFAULT NULL COMMENT '作者',
  `article_content` longtext COMMENT '文章内容',
  `article_img` varchar(255) DEFAULT NULL COMMENT '预览图片',
  `article_state` int(10) unsigned DEFAULT NULL COMMENT '文章状态 0草稿 1发布 2回收站',
  `page_view` int(20) unsigned DEFAULT NULL COMMENT '浏览量',
  `praise` int(255) DEFAULT NULL COMMENT '点赞',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  `update_date` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for article_category
-- ----------------------------
DROP TABLE IF EXISTS `article_category`;
CREATE TABLE `article_category` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(20) unsigned NOT NULL COMMENT '文章id',
  `category_id` int(20) unsigned NOT NULL COMMENT '分类id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=379 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for article_tag
-- ----------------------------
DROP TABLE IF EXISTS `article_tag`;
CREATE TABLE `article_tag` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `article_id` int(20) unsigned NOT NULL COMMENT '文章id',
  `tag_id` int(20) NOT NULL COMMENT '标签id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for blogroll
-- ----------------------------
DROP TABLE IF EXISTS `blogroll`;
CREATE TABLE `blogroll` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT '主键(ID)',
  `article_id` int(20) DEFAULT NULL,
  `order` int(10) DEFAULT NULL COMMENT '外链顺序(排序)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键(ID)',
  `name` varchar(100) DEFAULT NULL COMMENT '分类名',
  `alias` varchar(100) DEFAULT NULL COMMENT '别名(URL分类)',
  `count` int(20) DEFAULT NULL COMMENT '文章数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键(ID)',
  `author_name` varchar(50) NOT NULL COMMENT '用户名',
  `author_url` varchar(255) NOT NULL COMMENT '用户网址',
  `author_email` varchar(50) NOT NULL COMMENT '邮箱',
  `content` text NOT NULL COMMENT '评论内容',
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '留言时间',
  `ip` varchar(20) NOT NULL COMMENT '用户留言IP',
  `article_id` int(20) unsigned NOT NULL COMMENT '对应文章',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for multimedia
-- ----------------------------
DROP TABLE IF EXISTS `multimedia`;
CREATE TABLE `multimedia` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键(id)',
  `file_name` varchar(100) DEFAULT NULL COMMENT '文件名',
  `file_type` int(20) DEFAULT '0' COMMENT '文件类型',
  `file_url` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建日期',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for tag
-- ----------------------------
DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '组件(ID)',
  `name` varchar(100) DEFAULT NULL COMMENT '标签名',
  `alias` varchar(100) DEFAULT NULL,
  `count` int(20) DEFAULT NULL COMMENT '标签文章数量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键(ID)',
  `user_login` varchar(50) NOT NULL COMMENT '用户名',
  `user_pass` varchar(255) NOT NULL COMMENT '密码',
  `user_nicename` varchar(50) NOT NULL COMMENT '昵称',
  `user_email` varchar(50) NOT NULL COMMENT '邮箱',
  `user_status` int(10) NOT NULL COMMENT '用户状态  0无效   1有效',
  `user_right` int(10) NOT NULL COMMENT '用户权限 0为用户 1为管理员',
  `user_registered` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键(ID)',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `user_pass` varchar(255) NOT NULL COMMENT '密码',
  `user_nicename` varchar(50) NOT NULL COMMENT '昵称',
  `user_email` varchar(50) NOT NULL COMMENT '邮箱',
  `user_status` int(10) NOT NULL COMMENT '用户状态',
  `user_right` int(10) NOT NULL COMMENT '用户权限',
  `user_registered` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for web_config
-- ----------------------------
DROP TABLE IF EXISTS `web_config`;
CREATE TABLE `web_config` (
  `id` int(10) unsigned NOT NULL COMMENT '主键',
  `title` varchar(100) DEFAULT NULL COMMENT '网站标题',
  `keywords` varchar(255) DEFAULT NULL COMMENT 'SEO 关键字',
  `description` varchar(255) DEFAULT NULL COMMENT 'SEO  网站描述',
  `favicon` varchar(255) DEFAULT NULL COMMENT '网站 ICO 图标',
  `logo_img` varchar(255) DEFAULT NULL COMMENT '网站LOGO 地址',
  `theme_path` varchar(255) DEFAULT NULL COMMENT '主题路径',
  `domain_name` varchar(255) DEFAULT NULL COMMENT '网站域名',
  `short_name` varchar(255) DEFAULT NULL COMMENT '多说二级域名',
  `icp` varchar(255) DEFAULT NULL COMMENT '网站备案号',
  `pageSize` int(11) DEFAULT NULL COMMENT '每页显示文章数量',
  `page_view` int(20) DEFAULT NULL COMMENT '网站访问量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
